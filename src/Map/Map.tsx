import { useDebugValue, useEffect, useState } from "react";
import { GoogleMap, LoadScript, Autocomplete, MarkerF } from "@react-google-maps/api";

import "./map.css";

const containerStyle = {
    width: "400px",
    height: "400px",
};

type Location = google.maps.LatLngLiteral;

const createPos = (lat: number, lng: number) => ({ lat: lat, lng: lng });

export const Map = () => {
    const [autocomplete, setAutoComplete] = useState<google.maps.places.Autocomplete>();
    const [currentPos, setCurrentPos] = useState<Location>(createPos(0, 0));
    useDebugValue(currentPos);

    useEffect(() => {
        getCurrentPos();
    }, []);

    const getCurrentPos = () => {
        navigator.geolocation.getCurrentPosition(
            ({ coords: { latitude, longitude } }) => setCurrentPos(createPos(latitude, longitude)),
            () => setCurrentPos(createPos(0, 0))
        );
    };

    const onLoad = (autocomplete: google.maps.places.Autocomplete) => setAutoComplete(autocomplete);

    const onPlaceChanged = () => {
        if (autocomplete) {
            setCurrentPos((prevPos) => ({
                lat: autocomplete.getPlace().geometry?.location?.lat() ?? prevPos.lat,
                lng: autocomplete.getPlace().geometry?.location?.lng() ?? prevPos.lng,
            }));
        } else {
            throw Error("Autocomplete is not loaded yet!");
        }
    };

    return (
        <LoadScript
            googleMapsApiKey={import.meta.env.GOOGLE_MAPS_API_KEY ? import.meta.env.GOOGLE_MAPS_API_KEY : "AIzaSyC4bjcLNIKnWiNMyzQxshLjfTWhd3PzKsE"}
            libraries={["places"]}
        >
            <Autocomplete onLoad={onLoad} onPlaceChanged={onPlaceChanged}>
                <input className="autocomplete" type="text" placeholder="Customized your placeholder" />
            </Autocomplete>
            <GoogleMap mapContainerStyle={containerStyle} center={currentPos} zoom={10}>
                <>
                    <MarkerF position={currentPos}></MarkerF>
                </>
            </GoogleMap>
        </LoadScript>
    );
};
